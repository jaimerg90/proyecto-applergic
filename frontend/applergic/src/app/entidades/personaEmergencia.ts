export class PersonaEmergencia{
  public _id: String;
  public usuario: string;
  public nombre: string;
  public email: string;
  public movil: string;
  public compania: string;
  public poliza: string;
}