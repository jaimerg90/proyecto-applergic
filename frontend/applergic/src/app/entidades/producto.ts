export class Producto {
    nombre: string;
    ingredientes: String[];
    codigoBarras: string;
    imagen: string;
}
