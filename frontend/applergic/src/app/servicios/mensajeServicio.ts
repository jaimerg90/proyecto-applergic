import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })

export class MensajeServicio {
    error: any;
    mensaje: string;
}